#include "widget.h"
#include "ui_widget.h"



Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);

    series = new QScatterSeries();

    chart = new QChart();
    chart->addSeries(series);
    chart->createDefaultAxes();
    chart->setTitle("Random Points");
    ui->graphicsView->setChart(chart);

    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(addPoint()));
    /* Conexiones de signals y slots*/
    connect(ui->start, SIGNAL(pressed()), this, SLOT(start()));
    connect(ui->stop, SIGNAL(pressed()), this, SLOT(stop()));
    connect(ui->horizontalSlider,SIGNAL(valueChanged(int)), this, SLOT(changeTime(int)));
}

void Widget::addPoint(){
    QRandomGenerator rand = QRandomGenerator::securelySeeded();
    series->append(rand.generateDouble(), rand.generateDouble());
}

void Widget::start(){
    /* this->time corresponde al tiempo definido
     * para la solución de la parte 2 de esta pregunta.
     * Si en este punto, solo tiene definido 1000ms fijos
     * como tiempo, igualmente puede tener su puntaje
     * completo
     */
    timer->start(this->time);
}

void Widget::stop(){
    timer->stop();
}

void Widget::changeTime(int time){
    this->time = time;
}

Widget::~Widget()
{
    delete ui;
}

